var map;
var markers = new Array();
var infoWindow;
var clusterer = null;
var latLngList = new Array();

//Ao carregar a DOM, inicializa o mapa
google.maps.event.addDomListener(window, 'load', initMap);

$(document).ready(function () {
    //alert('oi mapa');
});

// Vale do itajai = -26.9513908,-48.7621093,11.24z

/**
 * Função executada ao terminar de carregar a DOM. Inicializa o mapa e chama as funções subsequentes.
 */
function initMap() {
    var latitude = -26.9513908;
    var longitude = -48.7621093;
    var zoom = 11;

    //Opções básicas do mapa, centralizando no vale do itajai/litoral.
    var mapOptions = {
        center: {lat: latitude, lng: longitude},
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.SATELLITE
    };

    //Cria o objeto do mapa e atacha à div #mapa
    map = new google.maps.Map(document.getElementById('mapa'), mapOptions);

    console.log(estacoes);
    //Exibe os markers de problemas
    visualizarEstacoes(estacoes);
    //console.log(estaco);
}

/**
 * Exibe um loading no mapa, com overlay, evitando de o usuário poder clicar várias vezes enquanto carrega.
 * @param onEnd Executa ao terminar de exibir o loading
 */
function showMapLoading(onEnd) {
    $('#loading-mapa').fadeIn(300, function () {
        if (typeof onEnd != 'undefined')
            onEnd();
    });
}

/**
 * Remove o loading de cima do mapa
 * @param onEnd Executa ao terminar de remover o loading
 */
function hideMapLoading(onEnd) {
    $('#loading-mapa').fadeOut(300, function () {
        if (typeof onEnd != 'undefined')
            onEnd();
    });
}

/**
 * Centraliza o mapa nos limites dos LatLng passados como parâmetro
 * @param LatLngList lista com os LatLng dos markers
 */
function centerMap(LatLngList) {
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
        bounds.extend(LatLngList[i]);
    }
    map.fitBounds(bounds);
}

/**
 * Adiciona os markers para cada problema.
 * Cria o cluster para os markers.
 * Caso existem estacoes, centraliza o mapa nos limites do markers adicionados existentes.
 */
function visualizarEstacoes(e) {
    for (var x in e) {
        var response = e[x];
        adicionarEstacaoMapa(response);
    }
    clusterer = new MarkerClusterer(map, markers.slice(0), {
        maxZoom: 17,
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
    });
    if (latLngList.length > 0)
        centerMap(latLngList);
}

/**
 * Adiciona uma estacao no mapa, criando um marker e preparando a infowindow para exibi-lo quando clicada
 * @param estacao
 */
function adicionarEstacaoMapa(response) {
    var latitude = parseFloat(response.estacao.latitude);
    var longitude = parseFloat(response.estacao.longitude);
    var latLng = new google.maps.LatLng(latitude, longitude);
    latLngList.push(latLng);
    //var icon = 'web/img/marker-' + estacao.status + '.png';
    var marker = new google.maps.Marker({
        position: latLng,
        estacao_id: response.estacao.id,
        map: map,
        //icon: icon
    });
    google.maps.event.addListener(marker, 'click', function (e) {
        showMapLoading(function () {
            try {
                var contentString = '<div id="content">' +
                    '<p>' + response.estacao.nome + '</p>' +
                    '<p>Nivel ' + response.medicao.nivel + ' metros</p>' +
                    '</div>';
                if (infoWindow != null)
                    infoWindow.close();
                infoWindow = new google.maps.InfoWindow({
                    content: contentString
                });
                infoWindow.setContentHTML(contentString);
                infoWindow.open(map, marker);
                hideMapLoading();
            } catch (e) {
                console.warn(e);
                hideMapLoading();
            }

        });

    });

    marker.tooltipContent = '';
    marker.tooltipContent = '<div style="height: 500px">\
                            <p style="text-align: center">' + response.estacao.rio + '<p>\
                            <p> Nivel: ' + response.medicao.nivel + ' metros<p>\n\
                            </div>';

    google.maps.event.addListener(marker, 'mouseover', function (e) {
        var point = fromLatLngToPoint(marker.getPosition(), map);
        $('#marker-tooltip').html(marker.tooltipContent).css({
            'left': point.x,
            'top': point.y
        }).show();
    });

    google.maps.event.addListener(marker, 'mouseout', function (e) {
        $('#marker-tooltip').hide();
    });

//    if (!isNull(problema.icone)) {
//        marker.setIcon(problema.icone);
//    }
//
    markers.push(marker);
}

function fromLatLngToPoint(latLng, map) {
    var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
    var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
    var scale = Math.pow(2, map.getZoom());
    var worldPoint = map.getProjection().fromLatLngToPoint(latLng);
    return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
}
