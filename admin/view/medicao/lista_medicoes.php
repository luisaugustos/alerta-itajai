<div class="row">

    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading"><h3>Ultimas Medições</h3></div>
            <div class="panel-body">
                
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success" href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "mapaEstacoes") ?>"><span class="fa fa-map-marker"></span> Ver estaçoes no Mapa</a>
                    </div>
                </div>
                
                <table class="table table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                            <th>Estaçao</th>
                            <th>Cidade</th>
                            <th>Data Adicionado</th>
                            <th>Nivel</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($ultimas as $record) { ?>
                        
                            <?php $status = 'status'; ?>
                            <tr class="tr-problema">
                                <td class="<?= $status ?>"><?= $record->getEstacao() ?></td>
                                <td class="<?= $status ?>"><?= $record->getCidade()->nome ?></td>
                                <td class="<?= $status ?>"><?=date("d/m/Y H:i:s", strtotime($record->data_medicao)); ?></td>
                                <td class="<?= $status ?>">
                                    <span class="box-icon-status"><i class="fa fa-exclamation-triangle"></i> <?= $record->nivel_rio ?></span>
                                </td>
                                <td><a class="btn btn-default" href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "porEstacao", $record->estacao_id) ?>"><span class="fa fa-eye"></span></a></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <hr>

                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <!--a class="btn btn-primary" href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "create") ?>"><span class="glyphicon glyphicon-plus"></span> Adicionar</a-->
                    </div>
                </div>

            </div>
        </div>

    </div>
</div> <!-- /row -->

<style type="text/css">
    thead tr th {
        border: none !important;
    }

    .box-icon-status {
        display: inline-block;
        width: 130px;
        line-height: 25px;
        padding: 3px;
        text-align: center;
        border: 1px solid;
        border-radius: 5px;
    }
</style>
