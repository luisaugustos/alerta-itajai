<!--<script src="web/js/Chart.js"></script>-->
<!--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h4><?= $estacao->nome ?></h4></div>

			<div class="panel-body">
				<div id="chart_div"></div>
				<!--<div class="grafico">
					<canvas id="myChart" width="800" height="400"></canvas>
				</div>-->
				<table class="table table-striped table-hover table-responsive">
					<thead>
					<tr>
						<th>Data Mediçao</th>
						<th>Nivel</th>
					</tr>
					</thead>
					<tbody>

					<?php if ($ultimas) {
						foreach ($ultimas as $record) { ?>
							<?php $status = 'status'; ?>
							<tr>
								<td class="<?= $status ?>"><?= date("d/m/Y H:i:s", strtotime($record->data_medicao)); ?></td>
								<td class="<?= $status ?>"><?= $record->nivel_rio ?></td>
							</tr>
						<?php }
					}
					?>
					</tbody>
				</table>

			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	var ultimas = <?= json_encode($ultimas) ?>;
</script>
<style type="text/css">
	thead tr th {
		border: none !important;
	}
</style>
