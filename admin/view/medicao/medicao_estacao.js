/*setTimeout(function () {
 gerarGrafico();
 }, 100);*/

/*function gerarGrafico() {
 console.log(ultimas);

 var dadosGrafico = new Array();

 for (var x in ultimas) {
 var medicao = ultimas[x];
 //console.log(medicao);
 dadosGrafico.push([medicao.data_medicao,medicao.nivel_rio]);
 }

 var data = {
 "labels": labels.reverse(),
 "datasets": [{
 label: "Sugar intake",
 fillColor: "rgba(151,187,205,0.2)",
 strokeColor: "rgba(151,187,205,1)",
 pointColor: "rgba(151,187,205,1)",
 pointStrokeColor: "#fff",
 pointHighlightFill: "#fff",
 pointHighlightStroke: "rgba(151,187,205,1)",
 data: niveis.reverse()
 }]
 }
 console.log(data);

 var chartDisplay = new Chart(document.getElementById("myChart").getContext("2d")).Line(data);
 }*/

google.load("visualization", "1", {packages: ["corechart"]});
google.setOnLoadCallback(drawChart);

function drawChart() {
    var dadosGrafico = [];

    dadosGrafico.push(['Data', 'Nivel']);
    for (var x in ultimas) {
        var medicao = ultimas[x];
        dadosGrafico.push([medicao.data_medicao, parseFloat(medicao.nivel)]);
    }
    console.log(dadosGrafico);
    var data = google.visualization.arrayToDataTable(dadosGrafico);

    var options = {
        title: 'Medição',
        hAxis: {title: '', titleTextStyle: {color: '#333'}, direction: -1, slantedText: true, slantedTextAngle: 50},
        vAxis: {direction: 1, minValue: 0, viewWindowMode: 'explicit', viewWindow: {min: 0}},
        width: 940,
        height: 450,
        chartArea: {left: 70},
        pointSize: 5,
        legend: {alignment: 'center'}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}
