<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfwRQu89O6FwDIK5EF4taspV4V54bv3ls"></script>
<script type="text/javascript " src="<?= UrlUtil::getInstance()->filePathToUrl(DIR_APP . DS . 'web' . DS . 'js' . DS . 'markerclusterer.js') ?>"></script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Mapa Estaçoes</h3></div>
            <div class="panel-body container-map">
                <div id="mapa" class="mapa">
                </div>
                <div id="loading-mapa" style="display: none">
                    <span>
                        <i class="fa fa-spinner fa-spin fa-2x"></i><br><br>
                        Carregando
                    </span>
                </div>
                <div id="marker-tooltip"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	var estacoes = <?= json_encode($estacoes) ?>;
</script>
<style>
    .mapa {
        height: 600px;
        width: 100%;
    }
    .container-map {
		position: relative;
	}
    #loading-mapa {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		text-align: center;
		background: url(web/img/back.png) repeat;
	}

	#loading-mapa span {
		position: absolute;
		top: 50%;
		left: 50%;
		margin-top: -50px;
		margin-left: -135px;
		background: #e8e8e8;
		width: 270px;
		padding: 20px;
		text-align: center;
		font-size: 14px;
		-webkit-border-radius: 8px;
		-moz-border-radius: 8px;
		border-radius: 8px;
	}

	#marker-tooltip {
		display: none;
		position: absolute;
		/*width: 300px;*/
		height: 80px;
		background-color: #ffffff;
		padding: 10px;
		margin-left: 10px;
		margin-top: 5px;

		-webkit-border-radius: 0px 8px 8px 8px;
		-moz-border-radius: 0px 8px 8px 8px;
		border-radius: 0px 8px 8px 8px;
	}
</style>
