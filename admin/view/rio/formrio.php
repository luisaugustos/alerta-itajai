<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $model->id ? "Edição" : "Cadastro" ?> de Estacao </div>
            <div class="panel-body">

                <form class="form" id="form" role="form" method="post" action="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "save") ?>">

                    <div class="row">

                        <div class="col-md-6">

                            <input type="hidden" name="id" id="id" value="<?= $model->id ?>">

                            <div class="form-group">
                                <label for="nome" class="required">Nome:</label>
                                <input type="text" class="form-control required" id="nome" name="nome" placeholder="Nome" value="<?= $model->nome ?>">
                            </div>

                        </div>

                        <div class="col-md-6">

                        </div>
                    </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "") ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
                    </div>
                </div>
            </div>
        </div>





        </form>
    </div> <!-- /.panel-body -->
</div> <!-- /.panel-default -->

</div>