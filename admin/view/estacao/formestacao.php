<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $model->id ? "Edição" : "Cadastro" ?> de Estacao </div>
            <div class="panel-body">

                <form class="form" id="form" role="form" method="post" action="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "save") ?>">

                    <div class="row">

                        <div class="col-md-6">
                            <h4>Dados Estaçao</h4>
                            <hr>
                            <input type="hidden" name="id" id="id" value="<?= $model->id ?>">

                            <div class="form-group">
                                <label for="nome" class="required">Nome:</label>
                                <input type="text" class="form-control required" id="nome" name="nome" placeholder="Nome" value="<?= $model->nome ?>">
                            </div>

                            <div class="form-group">
                                <label for="webservice" class="required">URL Webservice:</label>
                                <input type="text" class="form-control" id="webservice" name="webservice" placeholder="http://..." value="<?= $model->webservice ?>"/>
                            </div>

                            <div class="form-group">
                                <label for="rio" class="required">Rio:</label>
                                <select id="rio" name="rio_id" class="required form-control">
                                    <option value="">Selecione o rio</option>
                                    <?php
                                    var_dump($rios);
                                    foreach ($rios as $rio) {
                                        $selected = $model->rio_id == $rio->id ? "selected" : "";
                                        ?>
                                        <option <?= $selected ?> value="<?= $rio->id ?>"><?= $rio->nome ?> </option>
                                    <?php } ?>
                                </select>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="latitude" class="required">Latitude:</label>
                                        <input type="text" class="form-control required" id="latitude" name="latitude" placeholder="" value="<?= $model->getLatitude() ?>"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="longitude" class="required">Longitude:</label>
                                        <input type="text" class="form-control required" id="longitude" name="longitude" placeholder="" value="<?= $model->getLongitude() ?>"/>
                                    </div>
                                </div>

                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ceops_estacao_id" class="">ID ESTAÇÃO CEOPS:</label>
                                        <input type="text" class="form-control" id="ceops_estacao_id" name="ceops_estacao_id" placeholder="" value="<?= $model->ceops_estacao_id ?>"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="epagri_estacao_id" class="">ID ESTAÇÃO EPAGRI :</label>
                                        <input type="text" class="form-control" id="epagri_estacao_id" name="epagri_estacao_id" placeholder="" value="<?= $model->epagri_estacao_id ?>"/>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <h4>Endereço</h4>
                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="cep" class="">CEP: </label>
                                        <input type="text" class="form-control" name="cep" placeholder="CEP" pattern="[0-9]{5}\-[0-9]{3}" value="<?= $model->cep ?>" />
                                    </div>
                                    <div class="col-md-7">
                                        <label for="estado" class="required">Estado: </label>
                                        <select class="form-control required" name="estado_id" id="estado">
                                            <option value="">Selecione um estado </option>
                                            <?php
                                            foreach ($estados as $estado) {
                                                $selected = ($model->getCidade() && $model->getCidade()->estado_id == $estado->id) ? "selected='selected'" : "";
                                                ?>
                                                <option <?= $selected ?> value="<?= $estado->id ?>"><?= $estado->nome ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="cidade" class="required">Cidade: </label>
                                <select class="form-control required" name="cidade_id" id="cidade" <?= $model->cidade_id ? '' : 'disabled="disabled"' ?>>
                                    <?php if ($model->getCidade() == null) { ?>
                                        <option value="">Selecione um estado </option>
                                    <?php } else { ?>
                                        <option value=""> Selecione uma cidade </option>
                                        <?php
                                        foreach ($cidades as $cidade) {
                                            $selected = $cidade->id == $model->cidade_id ? "selected='selected'" : '';
                                            ?>
                                            <option <?= $selected ?> value="<?= $cidade->id ?>"><?= $cidade->nome ?></option>	
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="logradouro" class="">Logradouro: </label>
                                <input type="text" class="form-control" name="logradouro" id="endereco" placeholder="Logradouro" value="<?= $model->logradouro ?>"/>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="bairro" class="">Bairro: </label>
                                        <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro" value="<?= $model->bairro ?>" />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="numero" class="">N°:</label>
                                        <input type="text" class="form-control" name="numero" id="numero" placeholder="N°" value="<?= $model->numero ?>"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
            </div>


            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "") ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div> <!-- /.panel-body -->
</div> <!-- /.panel-default -->

</div>