$(document).ready(function () {
	//Validação
	$("#form").validate({
		rules: {
			email: {email: true},
			senha: {
				required: function (element) {
					return $("#id").val() == "";
				}
			},
			repetir_senha: {equalTo: "#senha"},
		},
		messages: {
			repetir_senha: {
				equalTo: "A Confirmação da Senha não está igual a Senha."
			}
		}
	});
});