<div id="login" class="panel panel-default">

	<div id="login-titulo" class="panel-heading"><strong><?= CLIENTE ?> - Login</strong></div>

	<div id="login-form" class="panel-body">

		<div class="text-center">
			Entre com seus dados para acessar o sistema
		</div>

		<hr />

		<div style="margin: 0 20px">
			<form class="form-horizontal" id="form-login" role="form" method="post" action="<?= UrlUtil::getInstance()->createUrl("Usuario", "executeLogin") ?>">
				<div class="form-group">
					<label for="email" class="col-sm-3 control-label">E-mail:</label>
					<div class="col-sm-9">
						<input type="email" name="email" class="form-control" id="email" placeholder="E-mail" autofocus="true">
					</div>
				</div>
				<div class="form-group">
					<label for="senha" class="col-sm-3 control-label">Senha:</label>
					<div class="col-sm-9">
						<input type="password" name="senha" class="form-control" id="senha" placeholder="Senha">
					</div>
				</div>
				<div class="form-group">

					<div class="col-sm-offset-3 col-sm-9 text-right">
						<button type="submit" class="btn btn-success">Entrar</button>
						<button type="button" class="btn btn-primary">Esqueci a senha</button>
					</div>
				</div>
			</form>
		</div> <!-- /.content -->

	</div> <!-- /#login-form -->

</div> <!-- /#login -->