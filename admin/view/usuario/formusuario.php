<div class="row">

	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><?= $model->id ? "Edição" : "Cadastro" ?> de Usuário</div>
			<div class="panel-body">

				<form class="form" id="form" role="form" method="post" action="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "save") ?>">

					<div class="row">

						<div class="col-md-6">

							<input type="hidden" name="id" id="id" value="<?= $model->id ?>">

							<div class="form-group">
								<label for="nome" class="required">Nome:</label>
								<input type="text" class="form-control required" id="nome" name="nome" value="<?= $model->nome ?>">
							</div>

							<div class="form-group">
								<label for="email" class="required">E-mail:</label>
								<input type="email" class="form-control required" id="email" name="email" value="<?= $model->email ?>">
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="senha" class="required">Senha:</label>
										<input type="password" class="form-control" id="senha" name="senha">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="repetir_senha" class="required">Confirmação da Senha:</label>
										<input type="password" class="form-control" id="repetir_senha" name="repetir_senha">
									</div>
								</div>

							</div>


						</div>

						<div class="col-md-6">
							<?php if (isset($niveisAcesso)) {
								?>
								<div class="form-group">
									<label for="nivel_acesso" class="required">Nível Acesso:</label>
									<select id="nivel_acesso" name="nivel_acesso" class="required form-control">
										<option value="">Selecione</option>
										<?php
										foreach ($niveisAcesso as $id => $nivelAcesso) {
											$selected = $model->nivel_acesso == $id ? "selected" : ""
											?>
											<option <?= $selected ?> value="<?= $id ?>"><?= $nivelAcesso ?> </option>
										<?php } ?>
									</select>

								</div>
							<?php } ?>
							
						</div>

					</div>

					<hr>

					<div class="row">
						<div class="col-md-12 text-right">
							<a href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "") ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
							<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
						</div>
					</div>

				</form>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel-default -->

	</div>
</div> <!-- /.row -->