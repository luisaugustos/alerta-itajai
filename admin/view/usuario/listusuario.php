<div class="row">

	<div class="col-md-12">

		<div class="panel panel-default">
			<div class="panel-heading">Listagem de Usuários</div>
			<div class="panel-body">

				<form action="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "doFilter") ?>" method="post" role="form">

					<div class="row destaque-campos">
						<div class="col-md-3">
							<?= ViewUtils::createFilter('Nome', 'nome', $controller->getFilterValue('nome'), Filter::OPERATOR_ILIKE, Filter::TYPE_STRING, Filter::LIKE_BOTH_SIDE) ?>
						</div>
						<div class="col-md-3">

						</div>
						<div class="col-md-3">

						</div>
						<div class="col-md-3">

						</div>
					</div>

					<div class="row text-right destaque-controles">
						<div class="col-md-12">
							<button type="submit" name="action" value="filter" class="btn"><span class="glyphicon glyphicon-search"></span> Filtrar</button>
							<button type="submit" name="action" value="clear" class="btn"><span class="glyphicon glyphicon-asterisk"></span> Limpar Filtro</button>
						</div>
					</div>

				</form>
				<hr>

				<table class="table table-striped table-hover table-responsive">
					<thead>
					<tr>
						<th>Nome</th>
						<th>Usuário</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($records as $record) { ?>
						<tr>
							<td>
								<div class="data"><?= $record->nome ?></div>
							</td>
							<td>
								<div class="data"><?= $record->email ?></div>
							</td>
							<td class="text-right">
								<div class="btn-group">
									<a href="#" data-target="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										Opções
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "edit", array($record->id)) ?>" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Editar</a></li>
										<li><a href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "deleteModel", array($record->id)) ?>" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span> Remover</a></li>
									</ul>
								</div>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>

				<hr>

				<div class="row">
					<div class="col-md-6">
						<?= $pagination ?>
					</div>
					<div class="col-md-6 text-right">
						<a class="btn btn-primary" href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "create") ?>"><span class="glyphicon glyphicon-plus"></span> Adicionar</a>
					</div>
				</div>

			</div>
		</div>

	</div>
</div> <!-- /row -->