<?php

/**
 * Description of MedicaoFactoryFactory
 *
 * @author Luis Augusto Silva - luis.bc@hotmail.com
 */
class MedicaoFactory {

    public static function createMedicaoItajai($estacao_id, $timestamp, $nivel_rio) {
        $medicao = new MedicaoRio();
        $medicao->estacao_id = $estacao_id;
        $medicao->data_medicao = $timestamp;
        ($nivel_rio < 0 ? 0 : $nivel_rio);
        $medicao->nivel_rio = $nivel_rio;
        var_dump('NIVEL FROM DC ITAJAI SAVED');
        return MedicaoRioDao::getInstance()->saveModel($medicao);
    }
    
    public static function createMedicaoChuvaItajai($estacao_id, $timestamp, $precipitacao) {
        $medicaoChuva = new MedicaoChuva();
        $medicaoChuva->estacao_id = $estacao_id;
        $medicaoChuva->data_medicao = $timestamp;
        $medicaoChuva->acumulado = $precipitacao;
        
        $ultimaMedicao = MedicaoChuvaDao::getInstance()->getUltimaMedicao($estacao_id);
        $ultimaMedicao = date("Y-m-d H:i:s", strtotime($ultimaMedicao->data_medicao));
        if ($ultimaMedicao != $medicaoChuva->data_medicao) {
            var_dump('CHUVA FROM DC ITAJAI SAVED');
            return MedicaoChuvaDao::getInstance()->saveModel($medicaoChuva);
        }
    }

    public static function createMedicaoNivelCEOPS($estacao_id, $timestamp, $nivel_rio) {
        $medicaoRio = new MedicaoRio();
        $medicaoRio->estacao_id = $estacao_id;
        $medicaoRio->data_medicao = $timestamp;
        ($nivel_rio < 0 ? 0 : $nivel_rio);
        $medicaoRio->nivel_rio = $nivel_rio;

        $ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao_id);
        $ultimaMedicao = date("Y-m-d H:i:s", strtotime($ultimaMedicao->data_medicao));
        var_dump($ultimaMedicao);
        var_dump($medicaoRio->data_medicao);
        if ($ultimaMedicao != $medicaoRio->data_medicao) {
            var_dump('NIVEL FROM CEOPS SAVED');
            return MedicaoRioDao::getInstance()->saveModel($medicaoRio);
        }
    }

    public static function createMedicaoChuvaCEOPS($estacao_id, $timestamp, $precipitacao) {
        $medicaoChuva = new MedicaoChuva();
        $medicaoChuva->estacao_id = $estacao_id;
        $medicaoChuva->data_medicao = $timestamp;
        $medicaoChuva->acumulado = $precipitacao;

        $ultimaMedicao = MedicaoChuvaDao::getInstance()->getUltimaMedicao($estacao_id);
        $ultimaMedicao = date("Y-m-d H:i:s", strtotime($ultimaMedicao->data_medicao));
        if ($ultimaMedicao != $medicaoChuva->data_medicao) {
            var_dump('CHUVA FROM CEOPS SAVED');
            return MedicaoChuvaDao::getInstance()->saveModel($medicaoChuva);
        }
    }

    /*
     * Verifica se esta em horario de verao, se estiver corrige a data que vem do webservice
     * retorna adicionando +1 hora
     */

    public static function parseTimeToHorarioVerao($timestamp) {
        $now = time();
        $hora = localtime($now, true);
        $timestamp = str_replace('/', '-', $timestamp);
        if ($hora['tm_isdst']) {
            $timestamp = str_replace('/', '-', $timestamp);
            $horarioVerao = date('Y-m-d H:i:s', strtotime($timestamp) + 3600);
            return $horarioVerao;
        }
        return date('Y-m-d H:i:s', strtotime($timestamp));
    }

}
