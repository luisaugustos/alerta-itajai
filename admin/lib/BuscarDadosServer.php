<?php

/**
 * Classe para buscar os dados em fontes externas como webservices e conteudo de site.
 *
 * @author Luis Augusto Silva - luis.bc@hotmail.com
 */
class BuscarDadosServer {
    /*
     * Funcao para inicializar a busca nos webservices e sites.
     */

    public function run() {
        var_dump('iniciando Itajai');
        $this->getFromDefesaCivilItajai();
        var_dump('iniciando CEOPS');
        $this->getFromCEOPS();
        //TODO buscar webservice CEOPS
        //TODO buscar webservice EPAGRI
    }

    /*
     * Funcao para obter as ultimas atualizacoes de telemetria diretamente do site da defesa civil de itajai
     * Se a medicao ja estiver no banco ela é ignorada.
     * Obtem o nivel do rio e a hora da medicao.
     * http://defesacivil.itajai.sc.gov.br
     * 
     * @author Luis Augusto Silva - luis.bc@hotmail.com
     */

    public function getFromDefesaCivilItajai() {

        //Buscar Estacoes somente cadastradas no site da defesa civil itajai
        $estacoesItajai = EstacaoDao::getInstance()->getEstacoesDefesaCivilItajai();

        foreach ($estacoesItajai as $estacao) {
            $ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao->id);
            $ultimaMedicao = date("Y-m-d H:i:s", strtotime($ultimaMedicao->data_medicao));
            $url = $estacao->webservice;
            var_dump($url);

            try {
                //Busca a pagina pelo id da estaçao
                $content = file_get_contents($url, FALSE, NULL);
                $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
            } catch (Exception $e) {
                echo $e->getMessage();
            }

            if ($content) {
                // NOME ESTAÇAO
                $first_step = explode('<div class="modal_titulo">', $content);
                $second_step = explode("</div>", $first_step[1]);
                $nome_estacao = $second_step[0];

                //NIVEL DO RIO NO MOMENTO
                $nivel_rio_first = explode('<div class="imprime_valor">', $content);
                $nivel_rio = explode("</div>", $nivel_rio_first[1]);
                $nivel_rio = $nivel_rio[0];

                //CHUVA ATUAL
                $chuva_atual = preg_replace('/\\s+/', ' ', $nivel_rio_first[3]);
                $chuva_atual = str_replace('</div> <div class="nome_valor">Chuva Acumulada 12Hrs:</div>', '', $chuva_atual);
                $chuva_atual = trim($chuva_atual);

                //HORA DA OBTENÇAO DA ULTIMA MEDIÇAO
                $data_nivel_rio = explode("</div>", $nivel_rio_first[1]);
                $nivel_rio_timestamp = preg_replace('/\\s+/', ' ', $nivel_rio_first[2]);
                $nivel_rio_timestamp = str_replace('.', '', $nivel_rio_timestamp);
                $nivel_rio_timestamp = str_replace('</div> <div class="clear"></div> <div class="nome_valor">Chuva Atual:</div>', '', $nivel_rio_timestamp);

                //TIMESTAMP
                $timestamp = str_replace(' - ', ' ', $nivel_rio_timestamp);
                $timestamp = str_replace('/', '-', $timestamp);
                $timestamp = date('Y-m-d H:i:s', strtotime($timestamp));

                var_dump($timestamp, $ultimaMedicao);
                if ($timestamp && $nivel_rio) {
                    if ($ultimaMedicao !== $timestamp) {
                        //Finaliza e cria a mediçao no banco 
                        MedicaoFactory::createMedicaoItajai($estacao->id, $timestamp, $nivel_rio);
                        MedicaoFactory::createMedicaoChuvaItajai($estacao->id, $timestamp, $chuva_atual);
                    }
                }
            }
        }
    }

    public function getFromCEOPS() {
        //WEBSERVICE CEOPS NAO TEM HORARIO DE VERAO!!!;
        $estacoes = EstacaoDao::getInstance()->getEstacoesCEOPS();
        foreach ($estacoes as $estacao) {

            $url = $estacao->webservice;
            try {
                //Busca a pagina pelo id da estaçao
                $json = file_get_contents($url, FALSE, NULL);
                //$json = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
            } catch (Exception $e) {
                echo $e->getMessage();
            }

            if ($json) {
                $medicoes = json_decode($json); //transformado JSON em array do PHP
                $medicao = $medicoes[0]; // Usa somente o primeiro item do json transformado em array do PHP\
                $medicao->data = MedicaoFactory::parseTimeToHorarioVerao($medicao->data);

                var_dump($medicao->ds_cidade);
                if ($medicao->ds_ativo_chuva) {
                    //cria a mediçao no banco 
                    MedicaoFactory::createMedicaoChuvaCEOPS($estacao->id, $medicao->data, $medicao->vlr_precipitacao);
                }
                if ($medicao->ds_ativo_nivel && $medicao->vlr_nivel != 'OFF') {
                    //cria a mediçao no banco 
                    MedicaoFactory::createMedicaoNivelCEOPS($estacao->id, $medicao->data, $medicao->vlr_nivel);
                }
            }
        }
    }

// END FUNCTION getFromCEOPS()
}
