<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= CLIENTE ?></title>

		<base href="<?= BASE ?>" />

		<!-- Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

		<!-- Bootstrap -->
		<link href="web/css/bootstrap.min.css" rel="stylesheet">

		<!-- Font Awesome -->
		<link href="web/css/font-awesome.min.css" rel="stylesheet">

		<!-- Jquery UI -->
		<link href="web/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
		
		<!-- DateTimePicker -->
		<link href="web/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

		<!-- Styles -->
		<link href="web/css/media.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body style="display: none">

		<?php if (isset($notLoadMenu) && $notLoadMenu == true) { ?>
			<?= $content ?>
		<?php } else { ?>

			<!------------------------------ Topo ------------------------------>

			<nav class="navbar navbar-default" id="navbar" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Menus</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?= UrlUtil::getInstance()->createUrl("", "") ?>"><?= CLIENTE ?></a>
					</div>

					<?php $usuario = SessionUtils::getInstance()->get(USER_SESSION_INDEX) ?>
					<?php if ($usuario) { ?>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

							<div class="hidden-lg hidden-md hidden-sm">
								<ul class="nav navbar-nav">
								</ul>
							</div>

							<ul class="nav navbar-nav navbar-right">
								<li><a href="<?= UrlUtil::getInstance()->createUrl("Usuario", "logout") ?>"><span class="glyphicon glyphicon-off"></span> Sair</a></li>
							</ul>

						</div><!-- /.navbar-collapse -->

					<?php } ?>

				</div><!-- /.container-fluid -->
			</nav>

			<!------------------------------ Menu Lateral ------------------------------>
			<?php $menuController = new MenuController() ?>
			<div id="menu-lateral" class="hidden-xs">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">Menu</div>
							<div class="panel-body">
								<ul class="menu-itens">
									<?php $mController = new MenuController(); ?>
									<?php echo $mController->getMenus(System::getInstance()->getControllerName(), System::getInstance()->getMethodName());?>
<!--									<li><a href="<?= UrlUtil::getInstance()->createUrl('Home', 'index')?>">Home</a></li>
									<li><hr></li>
									<li><a href="<?= UrlUtil::getInstance()->createUrl('Municipe', 'retrieve')?>">Municipes</a></li>
									<li><a href="<?= UrlUtil::getInstance()->createUrl('Problema', 'retrieve')?>">Problemas</a></li>
									<li><a href="<?= UrlUtil::getInstance()->createUrl('Instituicao', 'retrieve')?>">Instituições</a></li>
									<li><a href="<?= UrlUtil::getInstance()->createUrl('Categoria', 'retrieve')?>">Categorias</a></li>-->
<!--									<li><hr></li>
									<li><a href="<?= UrlUtil::getInstance()->createUrl('Usuario', 'retrieve')?>"><span class="glyphicon glyphicon-user"></span> Usuários</a></li>-->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /#menu-lateral -->

			<!------------------------------ Principal ------------------------------>

			<div id="corpo">
				<div class="container-fluid">
					<?= $content ?>
				</div><!-- /.container-fluid -->
			</div><!-- /#copo -->


		<?php } ?>


		<!------------------------------ Modal Dialog ------------------------------>
		<div class="modal fade" id="modalDialog" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><?= CLIENTE ?></h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->


		<!------------------------------ JS ------------------------------>

		<!-- jQuery -->
		<script src="web/js/jquery.min.js"></script>

		<!-- Bootstrap -->
		<script src="web/js/bootstrap.min.js"></script>

		<!-- Plugins jQuery -->
		<script src="web/js/jquery.mask.js"></script>
		<script src="web/js/jquery-ui/jquery-ui.js"></script>
		<script type="text/javascript" src="web/js/jquery.mask-money.min.js"></script>

		<!-- Plugins jQuery (Validate) -->
		<script src="web/js/jquery.validate.min.js"></script>
		<script src="web/js/jquery.validate.additional-methods.min.js"></script>
		<script src="web/js/localization/messages_pt_BR.js"></script>

		<!-- Plugin DateTimePicker -->
		<script src="web/js/moment.js"></script>
		<script src="web/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Aplicação -->
		<script src="web/js/utils.js"></script>
		<script src="web/js/app.js"></script>
		
		<!-- Demais Scripts -->
		<?= $controller->getJsScripts() ?>
	</body>
</html>