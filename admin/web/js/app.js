var STATUS_OK = '3';
var STATUS_ERRO = '1';
var lastRequest = null;
$(document).ready(function () {

    //Após terminar o carregamento
    $(window).load(function () {

        $('body').fadeIn(100);

        var bodyHeight = $('body').height();
        var windowHeight = $(window).height();
        if (bodyHeight < windowHeight) {
            $('body').css('min-height', windowHeight);
        }
    });

    $(window).bind('beforeunload', function () {
        $('body').fadeOut(100);
    });

    $(window).resize(onWindowResize);

    componentes();
    mascaras();
    onWindowResize();

});

var gateway = "index.php";

function onWindowResize() {
    var w = $(window).innerWidth();

    if (w > 750) {
        $('#corpo').width(w - $('#menu-lateral').outerWidth() - 25);
    } else {
        $('#corpo').width(w - 25);
    }

}

function nAlert(text) {
    $('#modalDialog').find('.modal-body').html(text);
    $('#modalDialog').modal();
    $('#modalDialog .btn-default').focus();
}

function doAjaxRequest(controller, method, params, posts, onReturnFunction) {
    var params = isNull(params) ? "" : params;
    if (typeof params === 'Array') {
        params = params.join("/");
    }
	if (!isNull(lastRequest)) {
//		lastRequest.abort();
	}
    lastRequest = $.ajax({
        url: gateway,
        dataType: 'JSON',
        async: false,
        data: {
            c: controller,
            m: method,
            p: params,
            posts: posts
        },
        type: 'POST',
        success: onReturnFunction,
        error: function (p1, p2, p3) {
            nAlert('Ocorreu um erro inesperado no sistema, por favor, contacte o administrador.');
        }
    })

}

function isNull(val) {
    return (typeof (val) === 'undefined') || val === null;
}
function redirectTo(controller, method, params) {
    var paramsString = "";
    if (!isNull(params) && params instanceof Array) {
        paramsString = params.join("/");
    } else if (!isNull(params)) {
        paramsString = params;
    }
    var url = "" + controller + "/" + method + "/" + paramsString;
    window.location = url;

}
function mascaras() {
    $('.cep').mask('99999-999');
    $('.cpf').mask('999.999.999-99');
    $('.cnpj').mask('99.999.999/9999-99');
    $('.telefone').mask('(99) 9999-9999');
    $('.date').mask('99/99/9999');
    $('.datetime').mask('99/99/9999 99:99');
    $('.celular').mask("(99) 99999-999?9");
    $('.celular').on('focusout', function () {
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    });
    $('.moeda').maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', allowZero: true});
}

function componentes() {

    //---------- Input File ----------//
    //<input type="file" name="imagem" id="imagem" accept="image/*" class="form-control" />
    $('input[type=file].form-control').each(function () {
        var $parent = $(this).parent();
        var $file = $(this);
        var $button = $('<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-file"></span> Selecionar Arquivo</button>');
        var $inputText = $('<input class="form-control" type="text" placeholder="Arquivo" readonly />');
        var $inputGroup = $('<div class="input-group"><span class="input-group-btn file-component"></span></div>');
        $file.addClass('file-component-input');
        $file.removeClass('form-control');
        $file.on('change', function () {
            $inputText.val($(this).val());
        });
        $inputGroup.find('.file-component').first().append($file);
        $inputGroup.find('.file-component').first().append($button);
        $inputGroup.append($inputText);
        $parent.append($inputGroup);
    });

    //---------- Tooltip ----------//
    $('[data-toggle="tooltip"]').tooltip();

    //---------- Drop-down submenu ----------//
    $('a.dropdown-collapse').on('click', function (e) {
        $(this).parents('.dropdown').children('ul').find('.dropdown-menu').hide(0);
        var id = $(this).attr('href');
        $(id).toggle(0);
        e.preventDefault();
        e.stopPropagation();
    });
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).children('ul').find('.dropdown-menu').hide(0);
    });

    //---------- Validação ----------//
    $.validator.setDefaults({
        highlight: function (element, errorClass, validClass) {
            $(element).parent('div').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent('div').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if ($(element.parent('div')).hasClass('input-group')) {
                error.insertAfter(element.parent('div'));
            } else if ($(element.parent('span')).hasClass('file-component')) {
                error.insertAfter(element.parent('span').parent('div'));
            } else if ($(element).is(':checkbox')) {
                if ($(element.parent('label').parent('div')).hasClass('checkbox')) {
                    error.insertAfter(element.parent('label').parent('div'));
                }
            } else {
                error.insertAfter(element);
            }
        }
    });
    $.extend($.validator.methods, {
        number: function (value, element) {
            return this.optional(element)
            || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:[,.]\d+)?$/.test(value);
        }
    });
    $.validator.addClassRules("date", {
        dateITA: true
    });

    //---------- Tooltip ----------//
    $('.tooltip').tooltip();

    //---------- DateTimePicker ----------//
    $('.date').datetimepicker({
        pickTime: false,
        language: 'pt-br'
    });
    $('.time').datetimepicker({
        pickDate: false,
        language: 'pt-br'
    });
    $('.datetime').datetimepicker({
        language: 'pt-br'
    });

}