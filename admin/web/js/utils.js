function buscaCep(cep, onReturnFunction) {
	if (isNull(onReturnFunction)) {
		onReturnFunction = function(json) {
			if (json.methodReturn !== null) {
				var endereco = json.methodReturn;
				$("#bairro").val(endereco.bairro);
				$("#endereco").val(endereco.endereco);
				$("#estado").val(endereco.estado_id);
				buscaCidades($("#estado").val());
				$("#cidade").val(endereco.cidade_id);
				if (endereco.bairro !== '' && endereco.estado_id !== null) {
					$("#numero").focus();
				}
			}
		};
	}
	doAjaxRequest('Cep', 'findEnderecoByCep', cep,null, onReturnFunction);
}


function createOptionCidade(cidade) {
	var $option = $("<option/>");
	$option.attr('value', cidade.id).text(cidade.nome);
	return $option;
}

function buscaCidades(estadoId, elementCidadeId) {
	posts = {
		estado_id : estadoId,
	};
	elementCidadeId = isNull(elementCidadeId) ? "#cidade" : elementCidadeId;
	$(elementCidadeId).empty().append("<option>Carregando..</option>");
	doAjaxRequest('Cidade', 'getJsonCidades',null , posts, function(json) {
		if (json.methodReturn !== null) {
			$(elementCidadeId).empty().append("<option value=''>Selecione uma cidade...</option>");
			$(elementCidadeId).attr("disabled", false);
			for (var x in json.methodReturn) {
				var cidade = json.methodReturn[x];
				var opt = createOptionCidade(cidade);
				$(elementCidadeId).append(opt);
			}
		}
	});
}



