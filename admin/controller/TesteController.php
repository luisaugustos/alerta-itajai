<?php

	class TesteController extends Controller {

		public function init() {
			parent::init();
			$this->addPublicMethod("*");
		}
		
		public function estacao() {
            $estacao_id = 7;
            $result = MedicaoChuvaDao::getInstance()->getUltimas24Horas($estacao_id);
            echo $result["acumulado_total"];
            
            echo '<br>';
            
            $result = MedicaoChuvaDao::getInstance()->getUltimas48Horas($estacao_id);
            echo $result["acumulado_total"];
        }

	}

