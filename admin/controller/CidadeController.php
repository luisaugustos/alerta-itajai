<?php

	class CidadeController extends Controller {

		public function init() {
			parent::init();
			$this->addPublicMethod("getJsonCidades");
		}
		
		public function getJsonCidades() {
			$cidades = CidadeDao::getInstance()->findByAttributes(array("estado_id" => $this->getPost('estado_id')), null, 'nome');
			
			return $cidades;
		}

	}

?>
