<?php

class MedicaoController extends Controller {

    public function init() {
        $this->dao = MedicaoRioDao::getInstance();
        //$this->addPublicMethod("*");
        parent::init();
    }
    
    public function index() {
        $ultimasMedicoes = array();
        $estacoes = EstacaoDao::getInstance()->findAll();
        foreach ($estacoes as $estacao) { 
            $ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao->id);
            if ($ultimaMedicao) {
                array_push($ultimasMedicoes, $ultimaMedicao);
            }
        }
        //var_dump($ultimasMedicoes);
        $this->addVar('ultimas', $ultimasMedicoes);
        $this->setView('lista_medicoes', 'medicao');
        
    }
    
    public function porEstacao($estacao_id) {
        $estacao = EstacaoDao::getInstance()->findById($estacao_id);
        //$ultimasMedicoes = MedicaoRioDao::getInstance()->getUltimas24Horas($estacao_id);
        $ultimasMedicoes = MedicaoRioDao::getInstance()->getUltimas48Horas($estacao_id);
        $this->addVar('estacao', $estacao);
        $this->addVar('ultimas', $ultimasMedicoes);
        $this->setView('medicao_estacao', 'medicao');
        
    }
    
    public function mapaEstacoes() {
        $ultimasMedicoes = array();
        $estacoes = EstacaoDao::getInstance()->findAll();
        foreach ($estacoes as $estacao) { 
            $ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao->id);
            $estacao->getLatitude();
            $estacao->getLongitude();
            if ($ultimaMedicao) {
                array_push($ultimasMedicoes, array('medicao' => $ultimaMedicao, 'estacao' => $estacao));
            }
        }
        $this->addVar('estacoes', $ultimasMedicoes);
        $this->setView('mapa');
    }
    
}
