<?php

class UsuarioController extends CRUDController {

	public function init() {
		$this->dao = UsuarioDao::getInstance();
		$this->addPublicMethod('login');
		$this->addPublicMethod('executeLogin');
		$this->addPublicMethod('logout');
            
	}
	
	public function edit($id = null) {
		$usuarioAutenticado = UsuarioDao::getInstance()->getUsuarioAutenticado();
		parent::edit($id);
	}

	/**
	 * Método executado por padrão
	 * @throws HeadersAlreadySentException
	 */
	public function index() {
		if (SessionUtils::getInstance()->get(USER_SESSION_INDEX) == null) {
			$this->redirect('Usuario', 'login');
		} else {
			$this->redirect('Usuario', 'retrieve');
		}
	}

	/**
	 * Redireciona para a tela de Login
	 */
	public function login() {
		$this->addVar('notLoadMenu', true);
		$this->setView("login");
	}

	/**
	 * Busca o usuáio no banco de dados e armazena na sessão
	 * @throws HeadersAlreadySentException
	 */
	public function executeLogin() {

		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$senha = isset($_POST['senha']) ? $_POST['senha'] : '';

		$usuario = UsuarioDao::getInstance()->findByAttributes(array('email' => $email, 'senha' => hash('sha256',$senha)));
		if (count($usuario) == 1) {
			$usuarioAutenticado = $usuario[0];
			UsuarioDao::getInstance()->setUsuarioAutenticado($usuarioAutenticado);
			$this->redirect('Home');
		} else {
			$this->addError("E-mail ou senha incorretos");
			$this->login();
		}
	}

	/**
	 * Desautentica o usuário
	 * @throws HeadersAlreadySentException
	 */
	public function logout() {
		SessionUtils::getInstance()->clear();
		$this->redirect('Usuario', 'login');
	}

	/**
	 * Executa antes de efetuar o save no modelo
	 * @param Model $model
	 */
	public function beforeSave(Model &$model) {
		if ($this->getPost('senha')) {
			$model->senha = hash('sha256',$this->getPost('senha'));
		}
		Database::getInstance()->initTransaction();
	}

	/**
	 * @param Model $model
	 * @return array
	 */
	public function validateModel(\Model $model) {
		$errors = array();
		if ($model->nome == null) {
			$errors[] = "Campo NOME é obrigatório";
		}
		if ($model->email == null) {
			$errors[] = "Campo E-MAIL é obrigatório";
		}
		if ($model->senha == null && !$model->id) {
			$errors[] = "Campo SENHA é obrigatório";
		}
		if ($model->email) {
			$usuarios = UsuarioDao::getInstance()->findByAttributes(array("email" => $model->email));
			if (count($usuarios) > 0) {
				$errors[] = "E-MAIL informado já está cadastrado em nossa base de dados";
			}
		}
		return $errors;
	}

	/**
	 * Executado após o save do modelo
	 * @param Model $model
	 * @throws HeadersAlreadySentException
	 */
	public function afterSave(Model $model) {
		Database::getInstance()->commitTransaction();
		$this->redirect('Usuario', 'retrieve');
	}

}
