<?php

class EstacaoController extends CRUDController {

    public function init() {
        $this->dao = EstacaoDao::getInstance();
        parent::init();
    }

    public function edit($id = null) {
        $this->addVar('estados', EstadoDao::getInstance()->findAll('nome asc'));
        $this->addVar('rios', RioDao::getInstance()->findAll());
        if ($id !== null) {
            $model = $this->dao->findById($id);
            $this->addVar('cidades', CidadeDao::getInstance()->findByAttributes(array("estado_id" => $model->getCidade()->estado_id)));
        }
        parent::edit($id);
    }

    public function beforeSave(\Model &$model) {
        $model->cep = preg_replace('/[^0-9]/', '', $model->cep);
        parent::beforeSave($model);
    }

}
