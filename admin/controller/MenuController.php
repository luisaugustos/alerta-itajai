<?php

class MenuController extends Controller {

	public function init() {
		//TODO criar dao aqui
		parent::init();
	}

	public function getMenus($currentController, $currentMethod) {
		//Monta os MENUS
		$menus = array(
			array('name' => 'Home', 'controller' => 'Home', 'icon' => '<span class="glyphicon glyphicon-home"></span>'),
            array('name' => 'Ultimas Medições', 'controller' => 'Medicao', 'icon' => '<i class="fa fa-bar-chart"></i>'),
            array('name' => 'Estações de Telemetria', 'controller' => 'Estacao'),
            array('name' => 'Rios', 'controller' => 'Rio'),
		);
		$return = '';
		foreach ($menus as $menu) {
			$return .= $this->getMenu(
					$menu['name'], $menu['controller'], isset($menu['method']) ? $menu['method'] : '', array($currentController, $currentMethod), isset($menu['sub']) ? $menu['sub'] : null, isset($menu['icon']) ? $menu['icon'] : null);
		}
		return $return;
	}

	/**
	 * 
	 * @param type $name
	 * @param type $controllerName
	 * @param type $method
	 * @param array $current
	 * @param type $sub
	 * @param type $icon
	 * @param type $level
	 * @return string
	 */
	public function getMenu($name, $controllerName, $method = null, $current = array(), $sub = null, $icon = null, $level = 0) {
		if (is_array($sub)) {
			$submenus = $return = '';
			foreach ($sub as $menu) {
				$submenus .= $this->getMenu(
						$menu['name'], $menu['controller'], isset($menu['method']) ? $menu['method'] : '', $current, isset($menu['sub']) ? $menu['sub'] : null, isset($menu['icon']) ? $menu['icon'] : null, 1);
				$activeSubmenu = (strpos($submenus, 'class="active') !== false) ? 'active-sub' : '';
			}
			if ($sub != null && count($sub) > 0 && $submenus == '') {
				return '';
			}
			$idSubMenu = 'sub-menu-' . rand(0, 999) . '-' . substr(md5($name . rand(0, 999999)), -5);
			$return .= ($level == 0) ? '<li class="dropdown">' : '<li class="dropdown-submenu ' . $activeSubmenu . '">';
			if ($level == 0) {
				$return .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' . $icon . $name . ' <span class="fa fa-caret-right pull-right"></span></a>';
				$return .= '<ul class="dropdown-menu dropdown-menu-zero" role="menu">';
			} else {
				$return .= '<a href="#' . $idSubMenu . '" class="dropdown-collapse">' . $icon . $name . '</a>';
				$return .= '<ul class="dropdown-menu" role="menu" id="' . $idSubMenu . '">';
			}

			$return .= $submenus;

			$return .= '</ul></li>';
			return $return;
		} else {
			$user = SessionUtils::getInstance()->get(USER_SESSION_INDEX);
			$controllerFullName = $controllerName . 'Controller';
			$controllerObj = new $controllerFullName();
			//Só vai imprimir os menus se o usuário tiver acesso ao método inicial
			$methodCheck = $method == '' ? 'retrieve' : $method;
			if ($controllerObj->hasPermission($methodCheck, $user)) {
				$active = strtolower($current[0]) == strtolower($controllerName) && strtolower($current[1]) == strtolower($methodCheck) ? 'class="active"' : "";
				$return = '<li ' . $active . '><a href = "' . UrlUtil::getInstance()->createUrl($controllerName, $method) . '">';
				$return .= $icon ? $icon . ' ' : $icon;
				$return .= $name;
				$return .= '</a></li>';
				return $return;
			}
			return '';
		}
	}

}
