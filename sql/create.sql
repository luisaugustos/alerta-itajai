
CREATE TABLE rio (
	id serial not null primary key,
	nome varchar(80) not null,
	status integer
);

CREATE table usuario (
	id serial not null primary key,
	nome VARCHAR(80) not null,
	email varchar(100) not null,
	senha char(32) not null,
	celular bigint
);

CREATE TABLE estacao (
	id serial not null primary key,
	nome varchar (80) not null,
	webservice varchar (250),
	ponto geometry(Point,4326),
	logradouro varchar (100), 
	numero varchar (10),
	bairro varchar(80),
	cep varchar(7),
	cidade_id integer not null,
	rio_id integer not null,
	data_adicionado timestamp without time zone NOT NULL DEFAULT now(),
	foreign key (cidade_id) references cidade (id),
	foreign key (rio_id) references rio (id)
);

CREATE TABLE medicao_rio (
	id serial not null primary key, 
	estacao_id integer not null,
	data_medicao timestamp not null,
	nivel_rio double precision not null,
	data_adicionado timestamp without time zone NOT NULL DEFAULT now(),
	foreign key (estacao_id) references estacao (id)
);

CREATE TABLE medicao_chuva (
	id serial not null primary key, 
	estacao_id integer not null,
	data_medicao timestamp not null,
	acumulado double precision not null,
	data_adicionado timestamp without time zone NOT NULL DEFAULT now(),
	foreign key (estacao_id) references estacao (id)
);



