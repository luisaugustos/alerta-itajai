<?php

class Estacao extends Model implements JsonSerializable {

	public $id;
	public $nome;
	public $webservice;
	public $ponto;
	public $logradouro;
	public $numero;
	public $bairro;
	public $cep;
	public $cidade_id;
	public $rio_id;
	public $data_adicionado;
	public $epagri_estacao_id;
	public $ceops_estacao_id;

	private $cidade;
	private $rio;
	private $latitude;
	private $longitude;
	private $nivel_rio;
	private $data_medicao;
	private $data_medicao_rio_adicionado;

	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}

	public function setLongitude($longitude) {
		$this->longitude = $longitude;
	}

	public function getLatitude() {
		return $this->latitude;
	}

	public function getLongitude() {
		return $this->longitude;
	}

	public function setDataMedicaoRio($data_medicao) {
		$this->data_medicao = $data_medicao;
	}

	public function setDataMedicaoRioAdicionado($data_medicao_adicionado) {
		$this->data_medicao_rio_adicionado = $data_medicao_adicionado;
	}

	public function setNivelRio($nivel_rio) {
		$this->nivel_rio = $nivel_rio;
	}

	public function getNivelRio() {
		return $this->nivel_rio;
	}

	public function getDataMedicaoRio() {
		return $this->data_medicao;
	}

	public function getDataMedicaoRioAdicionado() {
		return $this->data_medicao_rio_adicionado;
	}

	public function getCidade($lazy = true) {
		if ($this->cidade_id && (!$lazy || $this->cidade == NULL)) {
			$this->cidade = CidadeDao::getInstance()->findById($this->cidade_id);
		}
		return $this->cidade;
	}

	public function getRio($lazy = true) {
		if ($this->rio_id && (!$lazy || $this->rio == NULL)) {
			$this->rio = RioDao::getInstance()->findById($this->rio_id);
		}
		return $this->rio->nome;
	}

	public function populateByArray($array, $updateEmpty = true, $prefix = null) {
		parent::populateByArray($array, $updateEmpty, $prefix);
		if (isset($array['latitude'])) {
			$this->setLatitude($array['latitude']);
		}
		if (isset($array['longitude'])) {
			$this->setLongitude($array['longitude']);
		}
		if (isset($array['nivel_rio'])) {
			$this->setNivelRio($array['nivel_rio']);
		}
		if (isset($array['data_medicao'])) {
			$this->setDataMedicaoRio($array['data_medicao']);
		}
		if (isset($array['data_medicao_adicionado'])) {
			$this->setDataMedicaoRioAdicionado($array['data_medicao_adicionado']);
		}
	}

	public function jsonSerialize() {
		$array = (array)$this;
		$array['latitude'] = $this->getLatitude();
		$array['longitude'] = $this->getLongitude();
		$array['cidade'] = $this->getCidade();
		$array['rio'] = $this->getRio();
		$array['nivel_rio'] = $this->getNivelRio();
		$array['data_medicao_rio'] = $this->getDataMedicaoRio();
		$array['data_medicao_rio_adicionado'] = $this->getDataMedicaoRioAdicionado();
		return $array;
	}


}
