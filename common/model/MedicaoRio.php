<?php

class MedicaoRio extends Model implements JsonSerializable{

	public $id;
	public $estacao_id;
    public $data_medicao;
	public $nivel_rio;
    public $data_adicionado;
    
    private $estacao;
    private $cidade;
    
    public function getEstacao($lazy = true) {
		if ($this->estacao_id && (!$lazy || $this->estacao == NULL)) {
			$this->estacao = EstacaoDao::getInstance()->findById($this->estacao_id);
		}
		return $this->estacao;
	}

    
    public function getCidade($lazy = false) {
		if ($this->estacao_id && (!$lazy || $this->estacao == NULL)) {
			$estacao = EstacaoDao::getInstance()->findById($this->estacao_id);
            $this->cidade = $estacao->getCidade();
		}
		return $this->cidade;
	}
    
    public function populateByArray($array, $updateEmpty = true, $prefix = null) {
        parent::populateByArray($array, $updateEmpty, $prefix);
    }

	public function jsonSerialize() {
		return array(
			'id' => $this->id,
			'estacao_id' => $this->estacao_id,
			'data_medicao' => $this->data_medicao,
			'data_adicionado' => $this->data_adicionado,
			'nivel' => $this->nivel_rio
		);
	}

}
