<?php

class MedicaoChuvaDao extends Dao {

    public function __construct() {
        $this->config('MedicaoChuva', 'medicao_chuva');
        parent::__construct();
    }

    private static $singleton;

    /**
     * @return EstadoDao
     */
    public static function getInstance() {
        if (self::$singleton == null) {
            self::$singleton = new MedicaoChuvaDao();
        }
        return self::$singleton;
    }
    
    /*
     * Obtem somente a ultima mediçao da estacao gravada no banco
     */
    public function getUltimaMedicao($estacao_id) {
        $this->addOrderBy('data_medicao desc');
        $this->setLimit(1);
        $ultima = $this->findByAttributes(array('estacao_id' => $estacao_id));
        
        if ($ultima > 0 && !empty($ultima)) {
            return $ultima[0];
        } 
    }
    
    public function getUltimas24Horas($estacao_id) {
        $hoje = date('Y-m-d H:i:s', time());
        $ontem =  date('Y-m-d H:i:s',strtotime('last day'));
        $query = "SELECT SUM(acumulado::decimal(9,2)) as acumulado_total FROM ".$this->tableName;
		$values = $this->prepareQuery($query, array('data_medicao >=' => $ontem, 'data_medicao <=' => $hoje, 'estacao_id' => $estacao_id));
		$arr = Database::getInstance()->fetchArray($query, $values);
        if ($arr > 0 && !empty($arr)) {
            return $arr[0];
        } 
    }
    
    public function getUltimas48Horas($estacao_id) {
        $hoje = date('Y-m-d H:i:s', time());
        $ontem =  date('Y-m-d H:i:s',strtotime('-2 days'));
        $query = "SELECT SUM(acumulado::decimal(9,2)) as acumulado_total FROM ".$this->tableName;
		$values = $this->prepareQuery($query, array('data_medicao >=' => $ontem, 'data_medicao <=' => $hoje, 'estacao_id' => $estacao_id));
		$arr = Database::getInstance()->fetchArray($query, $values);
        if ($arr > 0 && !empty($arr)) {
            return $arr[0];
        } 
    }
}
