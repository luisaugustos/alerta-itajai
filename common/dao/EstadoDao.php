<?php

class EstadoDao extends Dao {

    public function __construct() {
        $this->config('Estado', 'estado');
        parent::__construct();
    }

    private static $singleton;

    /**
     * @return EstadoDao
     */
    public static function getInstance() {
        if (self::$singleton == null) {
            self::$singleton = new EstadoDao();
        }
        return self::$singleton;
    }
    
}