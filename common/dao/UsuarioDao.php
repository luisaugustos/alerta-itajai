<?php

class UsuarioDao extends Dao {

    public function __construct() {
        $this->config('Usuario', 'usuario');
        parent::__construct();
    }

    private static $singleton;

    /**
     * @return UsuarioDao
     */
    public static function getInstance() {
        if (self::$singleton == null) {
            self::$singleton = new UsuarioDao();
        }
        return self::$singleton;
    }

    /**
     * 
     * @param type $email
     * @param type $senha
     */
    public function executeLogin($email, $senha) {
        $usuario = $this->findByAttributes(array('email' => $email, 'senha' => md5($senha)));
        if (count($usuario) > 0) {
            $usuarioAutenticado = $usuario[0];
            UsuarioDao::getInstance()->setUsuarioAutenticado($usuarioAutenticado);
        } else {
            throw new Exception("Usuário ou senha incorretos", "131");
        }
    }

    /**
     * 
     * @return Usuario
     */
    public function getUsuarioAutenticado() {
        $usuarioAutenticado = SessionUtils::getInstance()->get(USER_SESSION_INDEX);
        return $usuarioAutenticado;
    }

    public function setUsuarioAutenticado($usuario) {
        SessionUtils::getInstance()->set(USER_SESSION_INDEX, $usuario);
    }

    

}
