<?php

class MedicaoRioDao extends Dao {

    public function __construct() {
        $this->config('MedicaoRio', 'medicao_rio');
        parent::__construct();
    }

    private static $singleton;

    /**
     * @return EstadoDao
     */
    public static function getInstance() {
        if (self::$singleton == null) {
            self::$singleton = new MedicaoRioDao();
        }
        return self::$singleton;
    }
    
    /*
     * Obtem somente a ultima mediçao da estacao gravada no banco
     */
    public function getUltimaMedicao($estacao_id) {
        $this->addOrderBy('data_medicao desc');
        $this->setLimit(1);
        $ultima = $this->findByAttributes(array('estacao_id' => $estacao_id));
        if ($ultima > 0 && !empty($ultima)) {
            return $ultima[0];
        } 
    }

    public function getUltimas($estacao_id) {
        $this->addColumns(["extract(MINUTE FROM data_medicao)::varchar x"]);
        $this->addOrderBy('data_medicao desc');
		$this->setLimit(24);
        $ultimas = $this->findByAttributes(array('0' => "extract(MINUTE FROM data_medicao)::varchar = '0'"));
        if ($ultimas > 0 && !empty($ultimas)) {
            return $ultimas;
        }
    }

    public function getUltimas24Horas($estacao_id) {
        $hoje = date('Y-m-d H:i:s', time());
        $ontem =  date('Y-m-d H:i:s',strtotime('last day'));
        $this->addColumns(["extract(MINUTE FROM data_medicao)::varchar x"]);
        $this->addOrderBy('data_medicao desc');
        $ultimas = $this->findByAttributes(array('0' => "data_medicao >= '$ontem' "
                                                      . "AND data_medicao <= '$hoje' "
                                                      . "AND estacao_id = $estacao_id"
                                                      . "AND extract(MINUTE FROM data_medicao)::varchar = '0'"));
        if ($ultimas > 0 && !empty($ultimas)) {
            return $ultimas;
        } 
    }
    
    public function getUltimas48Horas($estacao_id) {
        $hoje = date('Y-m-d H:i:s', time());
        $ontem =  date('Y-m-d H:i:s',strtotime('-2 day'));
        $this->addColumns(["extract(MINUTE FROM data_medicao)::varchar x"]);
        $this->addOrderBy('data_medicao desc');
        $ultimas = $this->findByAttributes(array('estacao_id' => $estacao_id));
        $ultimas = $this->findByAttributes(array('0' => "data_medicao >= '$ontem' "
                                                      . "AND data_medicao <= '$hoje' "
                                                      . "AND estacao_id = $estacao_id "
                                                      . "AND extract(MINUTE FROM data_medicao)::varchar = '0'"));
        if ($ultimas > 0 && !empty($ultimas)) {
            return $ultimas;
        } 
    }

    public function lastResults($estacao_id) {
        $this->setLimit('20');
        $this->addOrderBy('data_medicao desc');
        $ultimas = $this->findByAttributes(array('estacao_id' => $estacao_id));
        if ($ultimas > 0 && !empty($ultimas)) {
            return $ultimas;
        }
    }
    
    
    
}