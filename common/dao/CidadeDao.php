<?php

class CidadeDao extends Dao {

    public function __construct() {
        $this->config('Cidade', 'cidade');
        parent::__construct();
    }

    private static $singleton;

    /**
     * @return CidadeDao
     */
    public static function getInstance() {
        if (self::$singleton == null) {
            self::$singleton = new CidadeDao();
        }
        return self::$singleton;
    }
    
}