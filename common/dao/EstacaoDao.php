<?php

class EstacaoDao extends Dao {

	public function __construct() {
		$this->config('Estacao', 'estacao');
		parent::__construct();
	}

	private static $singleton;

	/**
	 * @return EstacaoDao
	 */
	public static function getInstance() {
		if (self::$singleton == null) {
			self::$singleton = new EstacaoDao();
		}
		return self::$singleton;
	}

	public function getAllEstacoes() {
		$this->addColumns(['a.nivel_rio', 'a.data_medicao as data_medicao', 'a.data_adicionado as data_medicao_adicionado']);
		$this->addJoin('LEFT JOIN (select distinct on(estacao_id) *  FROM medicao_rio order by estacao_id, data_adicionado desc) a on estacao.id = a.estacao_id');
		return $this->findByAttributes(array('0' => 'data_medicao is not null'));

	}

	public function saveModel(\Model $model, $updateNull = true, $useDBFunctions = null) {
		if ($model->getLatitude() && $model->getLongitude()) {
			$model->ponto = "POINT(" . $model->getLongitude() . " " . $model->getLatitude() . ")";
			$useDBFunctions = array("ponto" => "st_geomfromtext(ponto,4326)");
		}

		return parent::saveModel($model, $updateNull, $useDBFunctions);
	}

	protected function getSelect($extraColumns = null) {
		return parent::getSelect(array(0 => "st_x(ponto) as longitude", 1 => "st_y(ponto) as latitude"));
	}

	public function getEstacoesDefesaCivilItajai() {
		$estacoes = $this->findByAttributes(array('cidade_id' => '4538', 'nome ilike ' => 'DC-%'));
		if ($estacoes > 0) {
			return $estacoes;
		}
	}

	public function getEstacoesCEOPS() {
		$estacoes = $this->findByAttributes(array(0 => 'ceops_estacao_id is not null'));
		if ($estacoes > 0) {
			return $estacoes;
		}
	}

	
	


}