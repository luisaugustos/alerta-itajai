<?php

class RioDao extends Dao {

    public function __construct() {
        $this->config('Rio', 'rio');
        parent::__construct();
    }

    private static $singleton;

    /**
     * @return RioDao
     */
    public static function getInstance() {
        if (self::$singleton == null) {
            self::$singleton = new RioDao();
        }
        return self::$singleton;
    }
    
}