<?php

//------------------------------ URL PARSE ------------------------------//
if (URL_AMIGAVEL === true && isset($_REQUEST['rawUrl'])) {
	$data = UrlUtil::getInstance()->unmaskURL($_REQUEST['rawUrl']);
	$controller = $data['controller'] ? $data['controller'] : DEFAULT_CONTROLLER;
	$method = $data['method'] ? $data['method'] : DEFAULT_METHOD;
	$params = $data['params'];
} else {
	$controller = isset($_REQUEST['c']) ? $_REQUEST['c'] : DEFAULT_CONTROLLER;
	$method = isset($_REQUEST['m']) ? $_REQUEST['m'] : DEFAULT_METHOD;
	$params = isset($_REQUEST['p']) ? $_REQUEST['p'] : '';
}

//Hifen to Camel Caps
if (strpos($controller, '-') !== false) {
	$controller = lcfirst(implode('',array_map('ucfirst', explode('-', $controller))));
}
if (strpos($method, '-') !== false) {
	$method = lcfirst(implode('',array_map('ucfirst', explode('-', $method))));
}
//------------------------------ Usuário Autenticado ------------------------------//
$user = SessionUtils::getInstance()->get(USER_SESSION_INDEX);

//------------------------------ Execute - Exceptions ------------------------------//
try {
	echo System::getInstance()->execute($controller, $method, $params);
} catch (Exception $ex) {
	try {
		if ($ex instanceof HasNoPermissionException) {
			if ($user) {
				System::getInstance()->execute('Home', 'index', 'error_300');
			} else {
				echo System::getInstance()->execute("Home", "index", array());
			}
		}
	} catch (Exception $ex) {
		erroLegal($ex->getCode(), $ex->getMessage(), $ex->getFile(), $ex->getLine(), $ex);
	}
}