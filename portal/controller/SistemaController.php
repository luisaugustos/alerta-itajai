<?php

/**
 * Description of SistemaController
 *
 * @author Ricardo
 */
class SistemaController extends Controller {

	public function init() {
		$this->addPublicMethod('exception');
		$this->addPublicMethod('erro');
	}

	public function exception($exception = null) {
		dump($exception);die();
		if (isset($_GET['devx'])) {
			$this->addVar('exception', $exception);
			if (!SessionUtils::getInstance()->get(USER_SESSION_INDEX)) {
				$this->addVar('notLoadMenu', true);
			}
			$this->setView('exception');
		}
	}

	public function erro($errno = null, $errstr = null, $errfile = null, $errline = null) {
		$this->addVar('errno', $errno);
		$this->addVar('errstr', $errstr);
		$this->addVar('errfile', $errfile);
		$this->addVar('errline', $errline);
		switch ($errno) {
			case E_USER_ERROR:
				$this->addVar('class', 'danger');
				break;

			case E_USER_WARNING:
				$this->addVar('class', 'warning');
				break;

			case E_USER_NOTICE:
				$this->addVar('class', 'info');
				break;

			default:
				$this->addVar('class', 'other');
				break;
		}
		if (!SessionUtils::getInstance()->get(USER_SESSION_INDEX)) {
			$this->addVar('notLoadMenu', true);
		}
		$this->setView('erro');
	}

}
