<?php

class RestController extends Controller {

	/**
	 *
	 * @var RestRequest 
	 */
	private $restRequest;

	public function init() {
		$this->addPublicMethod('anuncio');
		$this->addPublicMethod('anunciante');
        $this->addPublicMethod('cidade');
        $this->addPublicMethod('categoria');
		parent::init();
	}

	public function anuncio() {
		$this->executeRestRequest(func_get_args());
	}

	public function anunciante() {
		$this->executeRestRequest(func_get_args());
	}
    
	public function cidade() {
		$this->executeRestRequest(func_get_args());
	}
    
    public function categoria() {
		$this->executeRestRequest(func_get_args());
	}
    

	public function getAnunciante() {
		$email = $this->restRequest->getData('email');
		$senha = $this->restRequest->getData('senha');
		$facebookId = $this->restRequest->getData('facebook_id');
		if ($email && $senha) {
			$retorno = AnuncianteDao::getInstance()->executeLogin($email, $senha);
			var_dump($retorno);
			if ($retorno != null) {
				RESTLight::getInstance()->send200Response(json_encode($retorno));
			} else {
				throw new Exception('E-mail ou senha incorretos.');
			}
		} else if ($facebookId) {
			$retorno = AnuncianteDao::getInstance()->findByFacebookId($facebookId);
			if ($retorno) {
				RESTLight::getInstance()->send200Response(json_encode($retorno));
			} else {
				$anunciante = new Anunciante();
				$anunciante->populateByArray($this->restRequest->getData());
				$retorno = AnuncianteDao::getInstance()->saveModel($anunciante);
				RESTLight::getInstance()->send200Response(json_encode($retorno));
			}
		} else {
			throw new Exception("Por favor, informe e-mail e senha");
		}
		die();
	}

	public function getAnuncio() {
		$id = $this->restRequest->getData('id');
		$local = $this->restRequest->getData('local');
		if ($id) {
			$this->getAnuncioById($id);
		}
		if ($local == 'pagina_inicial') {
			$this->getAnunciosPaginaInicial();
		} else if ($local == 'busca') {
			$this->getAnunciosBusca();
		}
	}

	private function getAnuncioById($id) {
		$anuncio = AnuncioDao::getInstance()->findById($id);
		RESTLight::getInstance()->send200Response(json_encode(
						array('status' => 'ok',
							'anuncio' => $anuncio,
							'anuncios_relacionados' => AnuncioDao::getInstance()->findAnunciosRelacionados($anuncio))
		));
	}

	private function getAnunciosPaginaInicial() {
		
	}

	private function getAnunciosBusca() {
		
	}

	public function postAnunciante() {
		$anunciante = new Anunciante();
		$anunciante->populateByArray($this->restRequest->getData());
		$errors = AnuncianteDao::getInstance()->validateModel($anunciante);
		if ($errors == NULL || count($errors) == 0) {
			$anunciante = AnuncianteDao::getInstance()->saveModel($anunciante);
			$anunciante->senha = NULL;
			RESTLight::getInstance()->send200Response(json_encode(array('status' => 'ok', 'anunciante' => $anunciante)));
		} else {
			$errorsString = implode(",", $errors);
			throw new Exception($errorsString);
		}
	}
    public function getCidade() {
        $uf = $this->restRequest->getData('uf');
		$cidade = $this->restRequest->getData('cidade');
        if ($uf && $cidade) {
            $return = CidadeDao::getInstance()->findbyNomeAndUf($cidade, $uf);
            RESTLight::getInstance()->send200Response(json_encode($return));        
        }
        
    }
    
    public function getCategoria() {
        $id = $this->restRequest->getData('categoria_id');
        if ($id) {
            $return = CategoriaDao::getInstance()->findByAttributes(array('categoria_id' => $id));
            RESTLight::getInstance()->send200Response(json_encode($return));        
        }
        $return = CategoriaDao::getInstance()->findByAttributes(array('categoria_id is' => null));
        RESTLight::getInstance()->send200Response(json_encode($return));        
        
    }
    
	public function processRequest() {
		$this->restRequest = RESTLight::getInstance()->processRequest();
	}

	private function executeRestRequest($params) {
		try {
			$this->processRequest();
			$routedMethod = RESTLight::getInstance()->getRoutedMethod($this);
			call_user_func_array(array($this, $routedMethod), $params);
		} catch (MetodoRestInexistenteException $e) {
			RESTLight::getInstance()->sendResponse('404', "Página não encontrada");
			die();
		} catch (Exception $ex) {

			$json = array('status' => 'erro', 'msg' => $ex->getMessage());
			if ($ex->getCode() != '') {
				$json['detail'] = $ex->getCode();
			}
			RESTLight::getInstance()->sendResponse('500', json_encode($json));
			die();
		}
	}

}
