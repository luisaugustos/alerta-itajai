<?php

class HomeController extends Controller {

	public function init() {
		$this->addPublicMethod('index');
	}

	public function index() {
		$this->setView('home');
		$this->addVar('medicoes', EstacaoController::ultimasMedicoes());
	}

}
