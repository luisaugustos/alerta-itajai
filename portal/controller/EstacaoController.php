<?php

class EstacaoController extends Controller {

	public function init() {
		$this->addPublicMethod("ultimasMedicoes");
		$this->addPublicMethod("todas");
		$this->addPublicMethod("detalhado");
		parent::init();
	}

	public function ultimasMedicoes() {
		$ultimasMedicoes = array();
		$estacoes = EstacaoDao::getInstance()->findAll();
		foreach ($estacoes as $estacao) {
			$ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao->id);
			if ($ultimaMedicao) {
				array_push($ultimasMedicoes, $ultimaMedicao);
			}
		}
		return $ultimasMedicoes;
	}

	public function todas() {
		$ultimasMedicoes = array();
		$estacoes = EstacaoDao::getInstance()->findAll();
		foreach ($estacoes as $estacao) {
			$ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao->id);
			if ($ultimaMedicao) {
				array_push($ultimasMedicoes, $ultimaMedicao);
			}
		}
		$this->addVar('estacoes', $ultimasMedicoes);
		$this->setView('listestacao');
	}

	public function detalhado($estacao_id) {
		$estacao = EstacaoDao::getInstance()->findById($estacao_id);
		//$ultimasMedicoes = MedicaoRioDao::getInstance()->getUltimas24Horas($estacao_id);
		/*$ultimasMedicoes = MedicaoRioDao::getInstance()->getUltimas48Horas($estacao_id);*/
		$ultimasMedicoes = MedicaoRioDao::getInstance()->lastResults($estacao_id);
		$this->addVar('estacao', $estacao);
		$this->addVar('ultimas', $ultimasMedicoes);
		$this->setView('detalhes');
	}

	public function mapaEstacoes() {
		$ultimasMedicoes = array();
		$estacoes = EstacaoDao::getInstance()->findAll();
		foreach ($estacoes as $estacao) {
			$ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao->id);
			$estacao->getLatitude();
			$estacao->getLongitude();
			if ($ultimaMedicao) {
				array_push($ultimasMedicoes, array('medicao' => $ultimaMedicao, 'estacao' => $estacao));
			}
		}
		$this->addVar('estacoes', $ultimasMedicoes);
		$this->setView('mapa');
	}

}
