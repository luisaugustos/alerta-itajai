<?php

//Inicializa a sessão
session_start();
//Altera o error reporting
error_reporting(E_ALL);
//Datetime São Paulo
date_default_timezone_set("America/Sao_Paulo");
//Define a pasta deste app
define('APP', basename(dirname(__FILE__)));
//Flag para debug
define('DEBUG', true);
//Define o error handler //TODO Construir uma função melhor para isto
//set_error_handler('erroLegal');

//Inclui o core do framework
require_once '../void/system/system.php';

//Constante do template da view
define('TEMPLATE_VIEW', DIR_APP . DS . 'web' . DS . 'index');
//Constante com o diretório de upload
define('UPLOAD_DIR', DIR_ROOT . DS . 'uploads');
//Constante que manterá um valor booleano para o tipo de requisição ser ajax ou não
define('IS_AJAX_REQUEST', (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'));

//Controle chamado por padrão
define('DEFAULT_CONTROLLER', 'Home');
//Método chamado por padrão
define('DEFAULT_METHOD', 'index');
//Número padrão de registros por listagem
define('DEFAULT_RECORDS_PER_PAGE', 20);

//Inclui as configurações do banco
require DIR_APP . DS . 'config-db.php';

//Nome da sessão de autenticação do usuáio
define('USER_SESSION_INDEX', 'site_autenticacao_portal');
//Nome do cliente
define('CLIENTE', 'Alerta Itajai');
//Se está utilizando urls amigáveis
define('URL_AMIGAVEL', true);

/**
 * Handler mais legal para os erros
 * @param $errno
 * @param $errstr
 * @param $errfile
 * @param $errline
 */
function erroLegal($errno, $errstr, $errfile, $errline, $exception = null) {

	echo '<div style="background-color: #f8f8f8; font-family: monospace; line-height: 19px; margin: 10px; padding: 10px">';
	echo $errstr . "<br>\n";
	echo basename($errfile) . ':' . $errline . "\n";
	echo '</div>';

	$file = file_get_contents($errfile);
	$file = explode("\n", $file);
	$slice = array_slice($file, $errline - 10, 21, true);
	echo '<div style="background-color: #f8f8f8; font-family: monospace; line-height: 19px; margin: 10px; padding: 10px">';
	foreach ($slice as $line => $code) {
		$highlighted = (str_replace(array('&lt;?php', '?&gt;'), '', highlight_string('<?php' . $code . "\n" . '?>', true)));
		if ($line == $errline - 1) {
			echo '<div style="background-color: #ececec">' . str_pad($line + 1, 5, ' ', STR_PAD_RIGHT) . ' ' . $highlighted . "</div>\n";
		} else {
			echo str_pad($line + 1, 5, ' ', STR_PAD_RIGHT) . ' ' . $highlighted . "\n";
		}
	}
	echo '</div>';
	if ($exception != null) {
		var_dump($exception);
	} else {
		var_dump(func_get_args());
	}
	die;
}