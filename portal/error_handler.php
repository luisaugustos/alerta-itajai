<?php

//------------------------------ Error Handler ------------------------------//
if (DEBUG) {
	set_error_handler('errorHandler');
} else {
	set_error_handler('friendlyErrorHandler');
}

function errorHandler($errno, $errstr, $errfile, $errline) {
	echo System::getInstance()->execute('Sistema', 'erro', array($errno, $errstr, $errfile, $errline));
	return true;
}

function friendlyErrorHandler($errno, $errstr, $errfile, $errline) {
	echo System::getInstance()->execute('Sistema', 'erro', array(false, 'Ocorreu um erro, entre em contato com o suporte técnico.', false, false));
	return true;
}