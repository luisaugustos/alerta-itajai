<div class="row">
	<div class="col-md-9">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div style="height: 200px" class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<h4>Noticias</h4>
								<hr>
							</div>
							<div class="col-md-6">
								<h4>Boletins Meteorológicos</h4>
								<hr>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="row">
			<div class="col-md-12 no-padding">
				<div class="panel panel-default">
					<div class="panel-heading">Ultimas Mediçoes</div>
					<div class="panel-body">
						<table class="table table-hover tabela-ultimas-medicoes">
							<thead>
							<tr>
								<th>Cidade</th>
								<th>Estacao</th>
								<th style="width: 65px">Nivel</th>
							</tr>
							</thead>
							<tbody>
							<?php
							foreach ($medicoes as $medicao) { ?>

								<?php $status = 'success'; ?>
								<tr class="tr-medicoes <?= $status ?>">
									<td class="medicao-cidade">
										<a href="#" data-toggle="tooltip" title="<?= $medicao->getCidade()->nome ?>">
											<?= $medicao->getCidade()->nome ?>
										</a>
									</td>
									<td class="medicao-estacao ">
										<a href="#" data-toggle="tooltip" title="<?= $medicao->getEstacao() ?>">
											<?= $medicao->getEstacao()->getRio() ?>
										</a>
									</td>
									<td class="medicao-nivel">
										<a href="#" data-toggle="tooltip" title="<?= sprintf("%0.2f", $medicao->nivel_rio) ?>">
											<?= sprintf("%0.2f", $medicao->nivel_rio) ?> <i class="fa fa-exclamation-triangle"> </i>
										</a>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>

</div>