<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">Estações</div>
			<div style="" class="panel-body">
				<table class="table table-hover tabela-ultimas-medicoes">
					<thead>
					<tr>
						<th>Cidade</th>
						<th>Rio</th>
						<th>Estacao</th>
						<th>Nivel</th>
						<th style="width: 150px"></th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach ($estacoes as $estacao) { ?>

						<?php $status = 'success'; ?>
						<tr class="tr-medicoes <?= $status ?>">
							<td class="medicao-cidade">
								<a href="#" data-toggle="tooltip" title="<?= $estacao->getCidade()->nome ?>">
									<?= $estacao->getCidade()->nome ?>
								</a>
							</td>
							<td class="medicao-estacao ">
								<a href="#" data-toggle="tooltip" title="<?= $estacao->getEstacao() ?>">
									<?= $estacao->getEstacao()->nome ?>
								</a>
							</td>
							<td class="medicao-estacao ">
								<a href="#" data-toggle="tooltip" title="<?= $estacao->getEstacao() ?>">
									<?= $estacao->getEstacao()->getRio() ?>
								</a>
							</td>
							<td class="medicao-nivel">
								<a href="#" data-toggle="tooltip" title="<?= sprintf("%0.2f", $estacao->nivel_rio) ?>">
									<?= sprintf("%0.2f", $estacao->nivel_rio) ?> <i class="fa fa-exclamation-triangle"> </i>
								</a>
							</td>
							<td>
								<a class="btn btn-default" href="<?= UrlUtil::getInstance()->createUrl($controller->getShortName(), "detalhado", $estacao->estacao_id) ?>"><span class="fa fa-eye"></span> Detalhes</a>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
