<div class="row">

	<div class="col-md-12">

		<div class="panel panel-default">
			<div class="panel-heading">Ops! Ocorreu um erro!</div>
			<div class="panel-body">

				<h4>Ocorreu um erro do tipo <strong><?= ucfirst($class) ?></strong></h4>

				<div class="alert alert-<?= $class ?>">
					<?php if ($errfile || $errline) { ?>
						<?= $errfile . ' : ' . $errline ?>
						<br><br>
					<?php } ?>
					<?php if ($errno) { ?>
						<?= $errno . ' : ' ?>
					<?php } ?>
					<?= $errstr ?>
				</div>

			</div>
		</div>
	</div>
</div>
