<div class="row">

	<div class="col-md-12">

		<div class="panel panel-default">
			<div class="panel-heading">Ops! Ocorreu um erro!</div>
			<div class="panel-body">

				<p>Mensagem de erro:</p>

				<div class="alert alert-danger">
					<?= $exception->getMessage() ?>
				</div>

				<?php if (DEBUG == true) { ?>

					<hr>
				
					<div class="btn btn-danger" id="btn-dump-level-1">Dump Level 1</div>
					<div class="btn btn-danger" id="btn-dump-level-2">Dump Level 2</div>
					<div class="btn btn-danger" id="btn-dump-level-complete">Dump Level Complete</div>

					<hr>
					
					<div class="dump-level" id="dump-level-1" style="background-color: #f9f2f4; padding: 10px">
						<?php dump($exception, 1); ?>
					</div>

					<div class="dump-level" id="dump-level-2" style="background-color: #f9f2f4; padding: 10px">
						<?php dump($exception, 2); ?>
					</div>

					<div class="dump-level" id="dump-level-complete" style="background-color: #f9f2f4; padding: 10px">
						<?php dump($exception); ?>
					</div>

				<?php } ?>
			</div>
		</div>
	</div>
</div>
