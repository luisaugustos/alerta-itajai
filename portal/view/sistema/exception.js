$(document).ready(function() {
	$('#dump-level-1').show(0);
	$('#btn-dump-level-1').on('click', function() {
		$('.dump-level').hide(0);
		$('#dump-level-1').show(0);
	});
	$('#btn-dump-level-2').on('click', function() {
		$('.dump-level').hide(0);
		$('#dump-level-2').show(0);
	});
	$('#btn-dump-level-complete').on('click', function() {
		$('.dump-level').hide(0);
		$('#dump-level-complete').show(0);
	});
});