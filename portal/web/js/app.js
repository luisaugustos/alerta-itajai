
function mascaras() {
    $('.cep').mask('99999-999');
    $('.cpf').mask('999.999.999-99');
    $('.cnpj').mask('99.999.999/9999-99');
    $('.telefone').mask('(99) 9999-9999');
    $('.date').mask('99/99/9999');
    $('.datetime').mask('99/99/9999 99:99');
    $('.celular').mask("(99) 99999-999?9");
    $('.celular').on('focusout', function () {
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    });
    $('.moeda').maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', allowZero: true});
    $('.numero').maskMoney({prefix: '', thousands: '', decimal: '', allowZero: true});
}



function nAlert(a, b, c) {
    $('#modal-alert').find('.modal-body').html(a);
    $('#modal-alert').modal();

    $('#modal-alert').off('shown.bs.modal');
    $('#modal-alert').on('shown.bs.modal', function (e) {
        $('#btn-modal-ok').focus();
    });

    $(document).off('click', '#btn-modal-ok');
    $(document).one('click', '#btn-modal-ok', function () {
        if (typeof b == 'function') {
            b();
        } else if (typeof c == 'function') {
            c();
        }
    });
}