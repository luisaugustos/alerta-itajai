<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Portal SADPREAI</title>
	<?php
	if (isset($metadata) && is_array($metadata)) {
		foreach ($metadata as $data) {
			?>
			<meta property="<?= $data->property ?>" content="<?= $data->content ?>">
			<?php
		}
	}
	?>
	<!-- Favicon -->

	<!-- Base URL -->
	<base href="<?= BASE ?>">

	<!-- Open Sans -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

	<!-- Bootstrap -->
	<link href="web/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="web/css/font-awesome.min.css" rel="stylesheet">
	<!-- App -->
	<link href="web/css/app.css" rel="stylesheet">

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<nav class="navbar navbar-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<a href="index.php" class="text-center">
				<img class="logo-sadpreai" src="web/img/sadepreailogo.png">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<form class="navbar-form navbar-left" role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Pesquisar">
					</div>
					<button type="submit" class="btn btn-default"><i class="fa fa-fw fa-search"></i></button>
				</form>
			</ul>
		</div>
	</div>
</nav>
<div class="subnav navbar navbar-default">
	<div class="navbar-inner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
				<ul class="nav navbar-nav">
					<li><a href="home">Inicio</a></li>
					<li><a href="estacao/todas">Dados das Estações</a></li>
					<li><a href="#">Dados Historicos</a></li>
					<li><a href="#">Mapa</a></li>
					<li><a href="#">Alertas</a></li>
					<li><a href="#">Nível do Rio</a></li>
					<li><a href="#">Catalogo de Bases Geográficas</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid">
	<?= $content ?>

</div>


<div class="footer">
	<div class="container">
		<h4>Mapa do portal</h4>
		<div class="row">
			<div class="col-md-3">
				<ul>
					<li><a href="#">Dados das Estações</a></li>
					<li><a href="#">Dados Historicos</a></li>
				</ul>

			</div>
			<div class="col-md-3">
				<ul>
					<li><a href="#">Nível do Rio</a></li>
					<li><a href="#">Mapa</a></li>
					<li><a href="#">Alertas</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<ul>
					<li><a href="#">Contato</a></li>
				</ul>

			</div>
		</div>
	</div>
	<br>


	<small class="text-muted">Este portal é de iniciativa de um Projeto de Pesquisa e Extensão do IFC Camboriú</small>
</div>
</div>

<!-- jQuery -->
<script src="web/js/lib/jquery.js"></script>
<script src="web/js/lib/jquery.mask-money.min.js"></script>
<script src="web/js/lib/jquery.maskedinput.min.js"></script>
<script src="web/js/lib/jquery.appear.js"></script>
<!-- Bootstrap -->
<script src="web/js/lib/bootstrap.min.js"></script>
<!-- Util -->
<script src="web/js/util.js"></script>
<!-- App -->
<script src="web/js/app.js"></script>
<script src="web/js/lib/jquery.validate.min.js"></script>
<!-- Demais -->
<?= $controller->getJsScripts() ?>

</body>
</html>