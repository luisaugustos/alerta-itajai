<?php

class UsuarioController extends RestController {

	public function init() {
		parent::init();
		$this->addPublicMethod('login');
		$this->addPublicMethod('cadastro');
	}

	public function cadastro() {
		$this->executeRestRequest(func_get_args());
	}

	public function login() {
		$this->executeRestRequest(func_get_args());
	}

	public function getLogin() {
		$email = $this->restRequest->getData('email');
		$senha = $this->restRequest->getData('senha');
		$usuario = UsuarioDao::getInstance()->findByAttributes(array('email' => $email, 'senha' => hash('sha256',$senha)));
		//var_dump(PostgreSQLPDO::interpolateQuery(Database::getInstance()->lastQuery,array('email' => $email, 'senha' => hash('sha256',$senha))));
		if ($usuario) {
			$json = array('status' => 'ok', 'usuario' => $usuario);
			RESTLight::getInstance()->send200Response($json);
		} else {
			$json = array('status' => 'erro', 'msg' => 'Usuário ou senha incorretos.');
			RESTLight::getInstance()->send200Response($json);
		}
	}

	/**
	 * Cadastro de usuário, tem que cadastrar um paciente também.
	 */
	public function postCadastro() {
		Database::getInstance()->initTransaction();
		$usuario = $this->saveUsuario();
		$paciente  = $this->savePaciente($usuario);
		$json = array('status' => 'ok', 'usuario' => $usuario, 'paciente' => $paciente);
		RESTLight::getInstance()->send200Response($json);
	}

	private function saveUsuario() {
		$usuario = new Usuario();
		$usuario->populateByArray($this->restRequest->getData());
		$erros = UsuarioDao::getInstance()->validateModel($usuario);
		if ($erros == null || count($erros) == 0) {
			$usuario->celular = preg_replace('/[^0-9]/', '', $usuario->celular);
			return UsuarioDao::getInstance()->saveModel($usuario);
		} else {
			$erros_string = implode("\n", $erros);
			throw new Exception($erros_string);
		}
	}

	private function savePaciente($usuario) {
		$paciente = new Paciente();
		$paciente->populateByArray($this->restRequest->getData());

		$erros = PacienteDao::getInstance()->validateModel($paciente);
		if ($erros == null || count($erros) == 0) {
			$paciente->usuario_paciente_id = $usuario->id;
			$paciente->data_nascimento = DateUtils::dateTimeToEnglishFormat($paciente->data_nascimento);
			$pacienteSalvo = PacienteDao::getInstance()->saveModel($paciente);
			PacienteDao::getInstance()->relatePacienteUsuario($pacienteSalvo,$usuario, true);
			$this->saveDoencas($pacienteSalvo);
			Database::getInstance()->commitTransaction();
			return $paciente;
		}  else {
			$erros_string = implode("\n", $erros);
			throw new Exception($erros_string);
		}
	}

	private function saveDoencas($paciente) {
		$doencasArray = $this->restRequest->getData('doencas');
		$doencas = [];
		if(is_array($doencasArray) && count($doencasArray) > 0) {
			foreach($doencasArray as $doenca) {
				$doencaObj = new Doenca();
				$doencaObj->populateByArray($doenca);
				if ($doencaObj->id == 0) {
					$doencaObj->confiavel = false;
					$doencaObj = DoencaDao::getInstance()->saveModel($doencaObj); // cria a doenca
				}
				$doencas[] = $doencaObj;
			}
			PacienteDoencaDao::getInstance()->associarPacienteDoencas($paciente, $doencas);
		}
	}



}
