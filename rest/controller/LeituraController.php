<?php

class LeituraController extends RestController {

	public function init() {
		parent::init();
		$this->addPublicMethod('estacao');
	}

	public function estacao() {
		$this->executeRestRequest(func_get_args());
	}


	public function getEstacao() {
		$estacaoId = $this->restRequest->getData('estacao_id');
		$chuvaAcumulada24 = MedicaoChuvaDao::getInstance()->getUltimas24Horas($estacaoId);
		$chuvaAcumulada48 = MedicaoChuvaDao::getInstance()->getUltimas48Horas($estacaoId);
		$ultimaMedicaoRio = MedicaoRioDao::getInstance()->getUltimas24Horas($estacaoId);
		$json = array(
			'status' => 'ok',
			'acumulado_24_horas' => $chuvaAcumulada24,
			'acumulado_48_horas' => $chuvaAcumulada48,
			'rio_medicao_24_horas' => $ultimaMedicaoRio
		);
		RESTLight::getInstance()->send200Response($json);
	}

	public function getFindById() {
		$estacaoId = $this->restRequest->getData('estacao_id');
		if ($estacaoId) {
			$estacao = EstacaoDao::getInstance()->findById($estacaoId);
			$json = array('status' => 'ok', 'estacao' => $estacao);
			RESTLight::getInstance()->send200Response($json);
		}
		else {
			$json = array('status' => 'erro', 'msg' => 'Nenhuma resultado encontrado');
			RESTLight::getInstance()->send200Response($json);
		}
	}

	public function getFindAll() {
		$estacoes = EstacaoDao::getInstance()->getAllEstacoes();
		if ($estacoes) {
			$json = array('status' => 'ok', 'estacoes' => $estacoes);
			RESTLight::getInstance()->send200Response($json);
		}
		else {
			$json = array('status' => 'erro', 'msg' => 'Nenhuma resultado encontrado');
			RESTLight::getInstance()->send200Response($json);
		}
	}

	public function getFindAllWithMedicao() {
		$ultimasMedicoes = array();
		$estacoes = EstacaoDao::getInstance()->findAll();
		foreach ($estacoes as $estacao) {
			$ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao->id);
			if ($ultimaMedicao) {
				array_push($ultimasMedicoes, $ultimaMedicao);
			}
		}
		if ($ultimasMedicoes) {
			$json = array('status' => 'ok', 'estacoes' => $ultimasMedicoes);
			RESTLight::getInstance()->send200Response($json);
		}
		else {
			$json = array('status' => 'erro', 'msg' => 'Nenhuma resultado encontrado');
			RESTLight::getInstance()->send200Response($json);
		}
	}

}
