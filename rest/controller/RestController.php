<?php
class RestController extends Controller {

	/**
	 *
	 * @var RestRequest
	 */
	protected $restRequest;


	public function init() {
		parent::init();
	}

	public function processRequest() {
		$this->restRequest = RESTLight::getInstance()->processRequest();
	}


	protected function executeRestRequest($params) {
		try {
			$this->processRequest();
			$routedMethod = RESTLight::getInstance()->getRoutedMethod($this);
			call_user_func_array(array($this, $routedMethod), $params);
			die();
		} catch (MetodoRestInexistenteException $e) {
			RESTLight::getInstance()->sendResponse('404', "Página não encontrada");
			die();
		} catch (Exception $ex) {
			$json = array('status' => 'erro', 'msg' => $ex->getMessage());
			if ($ex->getCode() != '') {
				$json['detail'] = $ex->getCode();
			}
			if (DEBUG) {
				$json['traceback'] = $ex->getTraceAsString();
			}
			RESTLight::getInstance()->sendResponse('500', json_encode($json));
			die();
		}
	}

}
