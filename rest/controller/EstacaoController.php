<?php

class EstacaoController extends RestController {

	public function init() {
		parent::init();
		$this->addPublicMethod('findAll');
		$this->addPublicMethod('findAllWithMedicao');
		$this->addPublicMethod('findById');
		$this->addPublicMethod('ultimasLeiturasPorEstacao');
	}

	public function findAll() {
		$this->executeRestRequest(func_get_args());
	}

	public function findAllWithMedicao() {
		$this->executeRestRequest(func_get_args());
	}

	public function findById() {
		$this->executeRestRequest(func_get_args());
	}

	public function ultimasLeiturasPorEstacao() {
		$this->executeRestRequest(func_get_args());
	}

	public function getUltimasLeiturasPorEstacao() {
		$estacaoId = $this->restRequest->getData('estacao_id');
		$ultimasMedicoes = MedicaoRioDao::getInstance()->getUltimas24Horas($estacaoId);
		if ($ultimasMedicoes) {
			$json = array('status' => 'ok', 'leituras' => $ultimasMedicoes);
			RESTLight::getInstance()->send200Response($json);
		}
		else {
			$json = array('status' => 'erro', 'msg' => 'Nenhuma resultado encontrado');
			RESTLight::getInstance()->send200Response($json);
		}
	}

	public function getFindById() {
		$estacaoId = $this->restRequest->getData('estacao_id');
		if ($estacaoId) {
			$estacao = EstacaoDao::getInstance()->findById($estacaoId);
			$json = array('status' => 'ok', 'estacao' => $estacao);
			RESTLight::getInstance()->send200Response($json);
		}
		else {
			$json = array('status' => 'erro', 'msg' => 'Nenhuma resultado encontrado');
			RESTLight::getInstance()->send200Response($json);
		}
	}

	public function getFindAll() {
		$estacoes = EstacaoDao::getInstance()->getAllEstacoes();
		if ($estacoes) {
			$json = array('status' => 'ok', 'estacoes' => $estacoes);
			RESTLight::getInstance()->send200Response($json);
		}
		else {
			$json = array('status' => 'erro', 'msg' => 'Nenhuma resultado encontrado');
			RESTLight::getInstance()->send200Response($json);
		}
	}

	public function getFindAllWithMedicao() {
		$ultimasMedicoes = array();
		$estacoes = EstacaoDao::getInstance()->findAll();
		foreach ($estacoes as $estacao) {
			$ultimaMedicao = MedicaoRioDao::getInstance()->getUltimaMedicao($estacao->id);
			if ($ultimaMedicao) {
				array_push($ultimasMedicoes, $ultimaMedicao);
			}
		}
		if ($ultimasMedicoes) {
			$json = array('status' => 'ok', 'estacoes' => $ultimasMedicoes);
			RESTLight::getInstance()->send200Response($json);
		}
		else {
			$json = array('status' => 'erro', 'msg' => 'Nenhuma resultado encontrado');
			RESTLight::getInstance()->send200Response($json);
		}
	}

}
