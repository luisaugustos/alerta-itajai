<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Headers: content-type");
//------------------------------ URL PARSE ------------------------------//
if (URL_AMIGAVEL === true && isset($_REQUEST['rawUrl'])) {
	$data = UrlUtil::getInstance()->unmaskURL($_REQUEST['rawUrl']);
	$controller = $data['controller'] ? $data['controller'] : DEFAULT_CONTROLLER;
	$method = $data['method'] ? $data['method'] : DEFAULT_METHOD;
	$params = $data['params'];
} else {
	$controller = isset($_REQUEST['c']) ? $_REQUEST['c'] : DEFAULT_CONTROLLER;
	$method = isset($_REQUEST['m']) ? $_REQUEST['m'] : DEFAULT_METHOD;
	$params = isset($_REQUEST['p']) ? $_REQUEST['p'] : '';
}

//------------------------------ Usuário Autenticado ------------------------------//
$user = SessionUtils::getInstance()->get(USER_SESSION_INDEX);

//------------------------------ Execute - Exceptions ------------------------------//

try {
	echo System::getInstance()->execute($controller, $method, $params);
} catch (HasNoPermissionException $ex) {
	if ($user) {
		System::getInstance()->execute('Home', 'index', 'error_300');
	} else {
		echo System::getInstance()->execute("Usuario", "login", array());
	}
} catch (PageNotFoundException $ex) {
	echo System::getInstance()->execute('Sistema', 'exception', array($ex));
} catch (DatabaseException $ex) {
	echo System::getInstance()->execute('Sistema', 'exception', array($ex));
} catch (FatalErrorException $ex) {
	//TODO tratar fatal error exception
} catch (WarningException $ex) {
	//TODO tratar warning exception
} catch (NoticeException $ex) {
	//TODO tratar notice exception
} catch (Exception $ex) {
	erroLegal($ex->getCode(), $ex->getMessage(), $ex->getFile(), $ex->getLine(), $ex);
}
